\context Staff = "baritono" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Barítono"
	\set Staff.shortInstrumentName = "B."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-baritono" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

%		\clef "bass"
		\clef "treble_15"
		\key e \major

		R1  |
		r2 r4 gis 8 fis  |
		e 4. b, 8 ~ b, 2  |
		r2 r4 b, 8 b,  |
%% 5
		cis 4. cis 8 ~ cis 2  |
		r2 r4 cis 8 cis  |
		b, 4 ( cis 8 ) cis ~ cis 2  |
		r2 cis 8 dis e 4 ~  |
		e 8 dis ~ dis 2 r4  |
%% 10
		r2 r4 e 8 e  |
		e 4. e 8 ~ e 2  |
		r2 r4 e 8 e  |
		e 4. e 8 ~ e 2  |
		r2 r4 cis 8 cis  |
%% 15
		b, 4 ( cis 8 ) cis ~ cis 2  |
		r4. fis, 8 cis dis e 4 ~  |
		e 8 dis ~ dis 2 r4  |
		r2 r4 a, 8 a,  |
		a, 4. a, a, 4  |
%% 20
		b, 4. b, b, 8 b,  |
		cis 4. cis cis 4  |
		e 4. e e 8 e  |
		a, 4. a, a, 4  |
		b, 4. b, b, 4  |
%% 25
		cis 4 cis 8 cis 4. cis 4  |
		gis, 4. gis, a, 8 a,  |
		a, 4. a, a, 4  |
		b, 4. b, b, 8 b,  |
		cis 4. cis cis 4  |
%% 30
		e 4. e e 8 e  |
		a, 4. a, a, 4  |
		b, 4. b, b, 4  |
		cis 4 cis 8 cis 4. cis 4  |
		gis, 4. gis, 4 gis, 8 gis, gis,  |
%% 35
		a, 8. a, a, 4 a, 8 a, a,  |
		b, 4 b, b, dis  |
		e 1  |
		R1*16  |
		r2 r8 gis, gis, gis,  |
%% 55
		a, 8. a, a, 4 a, 8 a, a,  |
		b, 4 b, b, dis  |
		e 1  |
		r2 r4 a, 8 a,  |
		a, 4. a, a, 4  |
%% 60
		b, 4. b, b, 8 b,  |
		cis 4. cis cis 4  |
		e 4. e e 8 e  |
		a, 4. a, a, 4  |
		b, 4. b, b, 4  |
%% 65
		cis 4 cis 8 cis 4. cis 4  |
		gis, 4. gis, a, 8 a,  |
		a, 4. a, a, 4  |
		b, 4. b, b, 8 b,  |
		cis 4. cis cis 4  |
%% 70
		e 4. e e 8 e  |
		a, 4. a, a, 4  |
		b, 4. b, b, 4  |
		cis 4 cis 8 cis 4. cis 4  |
		gis, 4. gis, 4 gis, 8 gis, gis,  |
%% 75
		a, 8. a, a, 4 a, 8 a, a,  |
		b, 4 b, b, dis  |
		e 1  |
		r2 r8 gis, gis, gis,  |
		a, 8. a, a, 4 a, 8 a, a,  |
%% 80
		b, 4 b, b, dis  |
		e 1  |
		r2 r8 gis, gis, gis,  |
		a, 8. a, a, 4 a, 8 a, a,  |
		b, 4 b, b, dis  |
%% 85
		e 1  |
		R1  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-baritono" {
		"Tú e" -- res san -- to,
		"Tú e" -- res fuer -- te,
		"Tú e" -- res dig -- no
		de to -- "do ho" -- nor.

		Te se -- gui -- mos,
		"te es" -- cu -- cha -- mos,
		"te a" -- ma -- re __  mos
		por siem -- pre ja __  más.

		%
		Can -- ta -- re -- mos pos -- tra -- dos
		por com -- ple -- "to en" -- tre -- ga -- dos;
		"te a" -- ma -- re -- mos por siem -- pre,
		de co -- ra -- zón y men -- te.

		Can -- ta -- re -- mos pos -- tra -- dos
		por com -- ple -- "to en" -- tre -- ga -- dos;
		"te a" -- ma -- re -- mos por siem -- pre,
		de co -- ra -- zón y men -- te.

		Tú e -- res nues -- tro Dios
		y nues -- tra vi -- "da es" pa -- ra ti.

		% con mujeres
		Tú e -- res nues -- tro Dios
		y nues -- tra vi -- "da es" pa -- ra ti.

		%
		Can -- ta -- re -- mos pos -- tra -- dos
		por com -- ple -- "to en" -- tre -- ga -- dos;
		"te a" -- ma -- re -- mos por siem -- pre,
		de co -- ra -- zón y men -- te.

		Can -- ta -- re -- mos pos -- tra -- dos
		por com -- ple -- "to en" -- tre -- ga -- dos;
		"te a" -- ma -- re -- mos por siem -- pre,
		de co -- ra -- zón y men -- te.

		Tú e -- res nues -- tro Dios
		y nues -- tra vi -- "da es" pa -- ra ti.

		Tú e -- res nues -- tro Dios
		y nues -- tra vi -- "da es" pa -- ra ti.

		Tú e -- res nues -- tro Dios
		y nues -- tra vi -- "da es" pa -- ra ti.
	}
>>
