\context Staff = "soprano" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Soprano"
	\set Staff.shortInstrumentName = "S."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-soprano" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

%		\clef "bass"
		\clef "treble"
		\key e \major

		R1*2  |
		r2 r4 gis' 8 fis'  |
		e' 4. b 8 ~ b 2  |
%% 5
		r2 r4 gis' 8 fis'  |
		e' 4. cis' 8 ~ cis' 2  |
		r2 r4 gis' 8 fis'  |
		e' 4 ( fis' 8 ) fis' ~ fis' 2  |
		r2 e' 8 fis' gis' 4 (  |
%% 10
		fis' 8 ) fis' ~ fis' 2 r4  |
		r2 r4 gis' 8 fis'  |
		e' 4. b 8 ~ b 2  |
		r2 r4 gis' 8 fis'  |
		e' 4. cis' 8 ~ cis' 2  |
%% 15
		r2 r4 gis' 8 fis'  |
		e' 4 ( fis' 8 ) fis' ~ fis' 2  |
		r4. b 8 e' fis' gis' 4 (  |
		fis' 8 ) fis' ~ fis' 2 r4  |
		R1*7  |
		r2 r4 e' 8 e'  |
		e' 4. e' e' 4  |
		fis' 4. fis' fis' 8 fis'  |
		gis' 4. e' gis' 4  |
%% 30
		gis' 4. gis' e' 8 e'  |
		e' 4. e' e' 4  |
		fis' 4. fis' fis' 4  |
		gis' 4 gis' 8 e' 4. gis' 4  |
		gis' 4. gis' 4 b 8 gis' b'  |
%% 35
		b' 8. gis' fis' 4 b 8 gis' b'  |
		b' 4 gis' fis' gis'  |
		e' 1  |
		r2. e' 8 e'  |
		dis' 8. e' e' 8 ~ e' 4 e' 8 e'  |
%% 40
		dis' 8. e' e' 8 -\staccato ~ e' 4 e' 8 e'  |
		dis' 8. e' e' 8 ~ e' 4 e' 8 fis'  |
		gis' 8. fis' e' 8 e' 4 -\staccato e' 8 e'  |
		dis' 8. e' e' 8 ~ e' 4 e' 8 e'  |
		dis' 8. e' e' 8 -\staccato ~ e' 4 e' 8 e'  |
%% 45
		dis' 8. e' e' 8 ~ e' 4 e' 8 fis'  |
		gis' 4 fis' e' e' 8 e'  |
		dis' 8. e' e' 8 ~ e' 4 e' 8 e'  |
		dis' 8. e' e' 8 -\staccato ~ e' 4 e' 8 e'  |
		dis' 8. e' e' 8 ~ e' 4 e' 8 fis'  |
%% 50
		gis' 4 fis' e' e' 8 e'  |
		dis' 8. e' e' 8 dis' 8. e' e' 8  |
		dis' 8. e' e' 8 e' 4 e' 8 e'  |
		dis' 8. e' e' 8 dis' 8. e' fis' 8  |
		gis' 8 gis' fis' e' 16 e' 8 r16 b 8 gis' b'  |
%% 55
		b' 8. gis' fis' 4 b 8 gis' b'  |
		b' 4 gis' fis' gis'  |
		e' 1  |
		r2 r4 e' 8 e'  |
		dis' 8. e' e' 8 ~ e' 4 e' 8 e'  |
%% 60
		dis' 8. e' e' 8 -\staccato ~ e' 4 e' 8 e'  |
		dis' 8. e' e' 8 ~ e' 4 e' 8 fis'  |
		gis' 8. fis' e' 8 e' 4 -\staccato e' 8 e'  |
		dis' 8. e' e' 8 ~ e' 4 e' 8 e'  |
		dis' 8. e' e' 8 -\staccato ~ e' 4 e' 8 e'  |
%% 65
		dis' 8. e' e' 8 ~ e' 4 e' 8 fis'  |
		gis' 4 fis' e' e' 8 e'  |
		dis' 8. e' e' 8 ~ e' 4 e' 8 e'  |
		dis' 8. e' e' 8 -\staccato ~ e' 4 e' 8 e'  |
		dis' 8. e' e' 8 ~ e' 4 e' 8 fis'  |
%% 70
		gis' 4 fis' e' e' 8 e'  |
		dis' 8. e' e' 8 dis' 8. e' e' 8  |
		dis' 8. e' e' 8 e' 4 e' 8 e'  |
		dis' 8. e' e' 8 dis' 8. e' fis' 8  |
		gis' 8 gis' fis' e' 16 e' 8 r16 b 8 gis' b'  |
%% 75
		b' 8. gis' fis' 4 b 8 gis' b'  |
		b' 4 gis' fis' gis'  |
		e' 1  |
		r2 r8 b gis' b'  |
		b' 8. gis' fis' 4 b 8 gis' b'  |
%% 80
		b' 4 gis' fis' gis'  |
		e' 1  |
		r2 r8 b gis' b'  |
		b' 8. gis' fis' 4 b 8 gis' b'  |
		b' 4 gis' fis' gis'  |
%% 85
		e' 1  |
		R1  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-soprano" {
		"Tú e" -- res san -- to,
		"Tú e" -- res fuer -- te,
		"Tú e" -- res dig -- no
		de to -- "do ho" -- nor.

		Te se -- gui -- mos,
		"te es" -- cu -- cha -- mos,
		"te a" -- ma -- re __  mos
		por siem -- pre ja __  más.

		% hombres:
		%Can -- ta -- re -- mos pos -- tra -- dos
		%por com -- ple -- "to en" -- tre -- ga -- dos;
		%"te a" -- ma -- re -- mos por siem -- pre,
		%de co -- ra -- zón y men -- te.

		% con hombres:
		Can -- ta -- re -- mos pos -- tra -- dos
		por com -- ple -- "to en" -- tre -- ga -- dos;
		"te a" -- ma -- re -- mos por siem -- pre,
		de co -- ra -- zón y men -- te.

		Tú e -- res nues -- tro Dios
		y nues -- tra vi -- "da es" pa -- ra ti.

		"Tú e" -- res el Se -- ñor, "tú e" -- res nues -- tro Rey,
		po -- de -- ro -- so Dios, Crea -- dor de lo que es;
		"tú e" -- res Em -- ma -- nuel, prín -- ci -- pe de paz,
		"tú e" -- res el que es el león de Ju -- dá.
		"Tú e" -- res buen pas -- tor, lla -- ve de Da -- vid,
		cor -- de -- ro de Dios que vi -- "ve has" -- "ta el" fin;
		"tú e" -- res al -- fa, o -- me -- ga, prin -- ci -- pio y fin;
		el me -- sí -- as que vi -- "no a" sal -- var y re -- di -- mir.

		Tú e -- res nues -- tro Dios
		y nues -- tra vi -- "da es" pa -- ra ti.

		"Tú e" -- res el Se -- ñor, "tú e" -- res nues -- tro Rey,
		po -- de -- ro -- so Dios, Crea -- dor de lo que es;
		"tú e" -- res Em -- ma -- nuel, prín -- ci -- pe de paz,
		"tú e" -- res el que es el león de Ju -- dá.
		"Tú e" -- res buen pas -- tor, lla -- ve de Da -- vid,
		cor -- de -- ro de Dios que vi -- "ve has" -- "ta el" fin;
		"tú e" -- res al -- fa, o -- me -- ga, prin -- ci -- pio y fin;
		el me -- sí -- as que vi -- "no a" sal -- var y re -- di -- mir.

		Tú e -- res nues -- tro Dios
		y nues -- tra vi -- "da es" pa -- ra ti.

		Tú e -- res nues -- tro Dios
		y nues -- tra vi -- "da es" pa -- ra ti.

		Tú e -- res nues -- tro Dios
		y nues -- tra vi -- "da es" pa -- ra ti.
	}
>>
