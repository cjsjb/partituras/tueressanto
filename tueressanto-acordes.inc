\context ChordNames
	\chords {
	\set majorSevenSymbol = \markup { "maj7" }
	\set chordChanges = ##t

	% intro
	e1 e1

	% tu eres santo...
	e1 e1 a1 a1 fis1:m fis1:m b1 b1

	% te seguimos...
	e1 e1 a1 a1 fis1:m fis1:m b1 b1

	% cantaremos...
	a1 b1 cis1:m e1 a1 b1 cis1:m gis1:m
	a1 b1 cis1:m e1 a1 b1 cis1:m gis1:m

	% tu eres nuestro dios...
	a1 b1 e1 e1

	% cantaremos...
	a1 b1 cis1:m e1 a1 b1 cis1:m gis1:m
	a1 b1 cis1:m e1 a1 b1 cis1:m gis1:m

	% tu eres nuestro dios...
	a1 b1 e1 e1

	% cantaremos...
	a1 b1 cis1:m e1 a1 b1 cis1:m gis1:m
	a1 b1 cis1:m e1 a1 b1 cis1:m gis1:m

	% tu eres nuestro dios...
	a1 b1 e1 e1

	% tu eres nuestro dios...
	a1 b1 e1 e1

	% tu eres nuestro dios...
	a1 b1 e1 e1
	}
