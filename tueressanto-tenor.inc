\context Staff = "tenor" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Tenor"
	\set Staff.shortInstrumentName = "T."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-tenor" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

%		\clef "bass"
		\clef "treble_8"
		\key e \major

		R1  |
		r2 r4 gis 8 fis  |
		e 4. b, 8 ~ b, 2  |
		r2 r4 gis 8 fis  |
%% 5
		e 4. cis 8 ~ cis 2  |
		r2 r4 gis 8 fis  |
		e 4 ( fis 8 ) fis ~ fis 2  |
		r2 e 8 fis gis 4 (  |
		fis 8 ) fis ~ fis 2 r4  |
%% 10
		r2 r4 gis 8 fis  |
		e 4. b, 8 ~ b, 2  |
		r2 r4 gis 8 fis  |
		e 4. cis 8 ~ cis 2  |
		r2 r4 gis 8 fis  |
%% 15
		e 4 ( fis 8 ) fis ~ fis 2  |
		r4. b, 8 e fis gis 4 (  |
		fis 8 ) fis ~ fis 2 r4  |
		r2 r4 b 8 b  |
		b 4. e b 4  |
%% 20
		b 4. fis b 8 b  |
		b 4. gis cis' 4  |
		b 4. e b 8 b  |
		b 4. e b 4  |
		b 4. fis b 4  |
%% 25
		b 4 b 8 gis 4. cis' 4  |
		b 4. e b 8 b  |
		b 4. e b 4  |
		b 4. fis b 8 b  |
		b 4. gis cis' 4  |
%% 30
		b 4. e b 8 b  |
		b 4. e b 4  |
		b 4. fis b 4  |
		b 4 b 8 gis 4. cis' 4  |
		b 4. dis 4 b, 8 gis b  |
%% 35
		b 8. gis fis 4 b, 8 gis b  |
		b 4 gis fis gis  |
		e 1  |
		R1*16  |
		r2 r8 b, gis b  |
%% 55
		b 8. gis fis 4 b, 8 gis b  |
		b 4 gis fis gis  |
		e 1  |
		r2 r4 b 8 b  |
		b 4. e b 4  |
%% 60
		b 4. fis b 8 b  |
		b 4. gis cis' 4  |
		b 4. e b 8 b  |
		b 4. e b 4  |
		b 4. fis b 4  |
%% 65
		b 4 b 8 gis 4. cis' 4  |
		b 4. e b 8 b  |
		b 4. e b 4  |
		b 4. fis b 8 b  |
		b 4. gis cis' 4  |
%% 70
		b 4. e b 8 b  |
		b 4. e b 4  |
		b 4. fis b 4  |
		b 4 b 8 gis 4. cis' 4  |
		b 4. dis 4 dis 8 gis b  |
%% 75
		b 8. gis e 4 e 8 gis b  |
		b 4 gis fis gis  |
		e 1  |
		r2 r8 dis gis b  |
		b 8. gis fis 4 e 8 gis b  |
%% 80
		b 4 gis fis gis  |
		e 1  |
		r2 r8 dis gis b  |
		b 8. gis fis 4 e 8 gis b  |
		b 4 gis fis gis  |
%% 85
		e 1  |
		R1  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-tenor" {
		"Tú e" -- res san -- to,
		"Tú e" -- res fuer -- te,
		"Tú e" -- res dig -- no
		de to -- "do ho" -- nor.

		Te se -- gui -- mos,
		"te es" -- cu -- cha -- mos,
		"te a" -- ma -- re __  mos
		por siem -- pre ja __  más.

		%
		Can -- ta -- re -- mos pos -- tra -- dos
		por com -- ple -- "to en" -- tre -- ga -- dos;
		"te a" -- ma -- re -- mos por siem -- pre,
		de co -- ra -- zón y men -- te.

		Can -- ta -- re -- mos pos -- tra -- dos
		por com -- ple -- "to en" -- tre -- ga -- dos;
		"te a" -- ma -- re -- mos por siem -- pre,
		de co -- ra -- zón y men -- te.

		Tú e -- res nues -- tro Dios
		y nues -- tra vi -- "da es" pa -- ra ti.

		% con mujeres:
		Tú e -- res nues -- tro Dios
		y nues -- tra vi -- "da es" pa -- ra ti.

		%
		Can -- ta -- re -- mos pos -- tra -- dos
		por com -- ple -- "to en" -- tre -- ga -- dos;
		"te a" -- ma -- re -- mos por siem -- pre,
		de co -- ra -- zón y men -- te.

		Can -- ta -- re -- mos pos -- tra -- dos
		por com -- ple -- "to en" -- tre -- ga -- dos;
		"te a" -- ma -- re -- mos por siem -- pre,
		de co -- ra -- zón y men -- te.

		Tú e -- res nues -- tro Dios
		y nues -- tra vi -- "da es" pa -- ra ti.

		Tú e -- res nues -- tro Dios
		y nues -- tra vi -- "da es" pa -- ra ti.

		Tú e -- res nues -- tro Dios
		y nues -- tra vi -- "da es" pa -- ra ti.
	}
>>
