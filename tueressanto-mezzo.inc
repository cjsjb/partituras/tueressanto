\context Staff = "mezzo" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Mezzosoprano"
	\set Staff.shortInstrumentName = "M."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-mezzo" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

%		\clef "bass"
		\clef "treble"
		\key e \major

		R1*2  |
		r2 r4 gis' 8 fis'  |
		e' 4. b 8 ~ b 2  |
%% 5
		r2 r4 b 8 b  |
		cis' 4. cis' 8 ~ cis' 2  |
		r2 r4 cis' 8 cis'  |
		b 4 ( cis' 8 ) cis' ~ cis' 2  |
		r2 cis' 8 dis' e' 4 ~  |
%% 10
		e' 8 dis' ~ dis' 2 r4  |
		r2 r4 e' 8 e'  |
		e' 4. e' 8 ~ e' 2  |
		r2 r4 e' 8 e'  |
		e' 4. e' 8 ~ e' 2  |
%% 15
		r2 r4 cis' 8 cis'  |
		b 4 ( cis' 8 ) cis' ~ cis' 2  |
		r4. fis 8 cis' dis' e' 4 ~  |
		e' 8 dis' ~ dis' 2 r4  |
		R1*7  |
		r2 r4 e' 8 e'  |
		e' 4. e' e' 4  |
		dis' 4. dis' dis' 8 dis'  |
		e' 4. e' e' 4  |
%% 30
		e' 4. e' e' 8 e'  |
		e' 4. e' e' 4  |
		dis' 4. dis' dis' 4  |
		e' 4 e' 8 e' 4. e' 4  |
		dis' 4. dis' 4 gis 8 e' e'  |
%% 35
		e' 8. e' e' 4 e' 8 e' e'  |
		dis' 4 dis' dis' dis'  |
		e' 1  |
		r2 r4 b 8 b  |
		cis' 8. cis' cis' 8 ~ cis' 4 cis' 8 cis'  |
%% 40
		b 8. b b 8 -\staccato ~ b 4 b 8 b  |
		cis' 8. cis' cis' 8 ~ cis' 4 cis' 8 dis'  |
		e' 8. dis' cis' 8 cis' 4 -\staccato b 8 b  |
		cis' 8. cis' cis' 8 ~ cis' 4 cis' 8 cis'  |
		b 8. b b 8 -\staccato ~ b 4 b 8 b  |
%% 45
		cis' 8. cis' cis' 8 ~ cis' 4 cis' 8 dis'  |
		e' 4 dis' cis' b 8 b  |
		cis' 8. cis' cis' 8 ~ cis' 4 cis' 8 cis'  |
		b 8. b b 8 -\staccato ~ b 4 b 8 b  |
		cis' 8. cis' cis' 8 ~ cis' 4 cis' 8 dis'  |
%% 50
		e' 4 dis' cis' b 8 b  |
		cis' 8. cis' cis' 8 cis' 8. cis' cis' 8  |
		b 8. b b 8 b 4 b 8 b  |
		cis' 8. cis' cis' 8 cis' 8. cis' dis' 8  |
		e' 8 e' dis' cis' 16 cis' 8 r16 e' 8 e' e'  |
%% 55
		e' 8. e' e' 4 e' 8 e' e'  |
		dis' 4 dis' dis' dis'  |
		e' 1  |
		r2 r4 b 8 b  |
		cis' 8. cis' cis' 8 ~ cis' 4 cis' 8 cis'  |
%% 60
		b 8. b b 8 -\staccato ~ b 4 b 8 b  |
		cis' 8. cis' cis' 8 ~ cis' 4 cis' 8 dis'  |
		e' 8. dis' cis' 8 cis' 4 -\staccato b 8 b  |
		cis' 8. cis' cis' 8 ~ cis' 4 cis' 8 cis'  |
		b 8. b b 8 -\staccato ~ b 4 b 8 b  |
%% 65
		cis' 8. cis' cis' 8 ~ cis' 4 cis' 8 dis'  |
		e' 4 dis' cis' b 8 b  |
		cis' 8. cis' cis' 8 ~ cis' 4 cis' 8 cis'  |
		b 8. b b 8 -\staccato ~ b 4 b 8 b  |
		cis' 8. cis' cis' 8 ~ cis' 4 cis' 8 dis'  |
%% 70
		e' 4 dis' cis' b 8 b  |
		cis' 8. cis' cis' 8 cis' 8. cis' cis' 8  |
		b 8. b b 8 b 4 b 8 b  |
		cis' 8. cis' cis' 8 cis' 8. cis' dis' 8  |
		e' 8 e' dis' cis' 16 cis' 8 r16 e' 8 e' e'  |
%% 75
		e' 8. e' e' 4 e' 8 e' e'  |
		dis' 4 dis' dis' dis'  |
		e' 1  |
		r2 r8 e' e' e'  |
		e' 8. e' e' 4 e' 8 e' e'  |
%% 80
		dis' 4 dis' dis' dis'  |
		e' 1  |
		r2 r8 e' e' e'  |
		e' 8. e' e' 4 e' 8 e' e'  |
		dis' 4 dis' dis' dis'  |
%% 85
		e' 1  |
		R1  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-mezzo" {
		"Tú e" -- res san -- to,
		"Tú e" -- res fuer -- te,
		"Tú e" -- res dig -- no
		de to -- "do ho" -- nor.

		Te se -- gui -- mos,
		"te es" -- cu -- cha -- mos,
		"te a" -- ma -- re __  mos
		por siem -- pre ja __  más.

		% hombres:
		%Can -- ta -- re -- mos pos -- tra -- dos
		%por com -- ple -- "to en" -- tre -- ga -- dos;
		%"te a" -- ma -- re -- mos por siem -- pre,
		%de co -- ra -- zón y men -- te.

		% con hombres:
		Can -- ta -- re -- mos pos -- tra -- dos
		por com -- ple -- "to en" -- tre -- ga -- dos;
		"te a" -- ma -- re -- mos por siem -- pre,
		de co -- ra -- zón y men -- te.

		Tú e -- res nues -- tro Dios
		y nues -- tra vi -- "da es" pa -- ra ti.

		"Tú e" -- res el Se -- ñor, "tú e" -- res nues -- tro Rey,
		po -- de -- ro -- so Dios, Crea -- dor de lo que es;
		"tú e" -- res Em -- ma -- nuel, prín -- ci -- pe de paz,
		"tú e" -- res el que es el león de Ju -- dá.
		"Tú e" -- res buen pas -- tor, lla -- ve de Da -- vid,
		cor -- de -- ro de Dios que vi -- "ve has" -- "ta el" fin;
		"tú e" -- res al -- fa, o -- me -- ga, prin -- ci -- pio y fin;
		el me -- sí -- as que vi -- "no a" sal -- var y re -- di -- mir.

		Tú e -- res nues -- tro Dios
		y nues -- tra vi -- "da es" pa -- ra ti.

		"Tú e" -- res el Se -- ñor, "tú e" -- res nues -- tro Rey,
		po -- de -- ro -- so Dios, Crea -- dor de lo que es;
		"tú e" -- res Em -- ma -- nuel, prín -- ci -- pe de paz,
		"tú e" -- res el que es el león de Ju -- dá.
		"Tú e" -- res buen pas -- tor, lla -- ve de Da -- vid,
		cor -- de -- ro de Dios que vi -- "ve has" -- "ta el" fin;
		"tú e" -- res al -- fa, o -- me -- ga, prin -- ci -- pio y fin;
		el me -- sí -- as que vi -- "no a" sal -- var y re -- di -- mir.

		Tú e -- res nues -- tro Dios
		y nues -- tra vi -- "da es" pa -- ra ti.

		Tú e -- res nues -- tro Dios
		y nues -- tra vi -- "da es" pa -- ra ti.

		Tú e -- res nues -- tro Dios
		y nues -- tra vi -- "da es" pa -- ra ti.
	}
>>
