\context Staff = "violin" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Violín"
	\set Staff.shortInstrumentName = "V."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-violin" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\clef "treble"
		\key e \major

		R1*10  |
		< e' gis' > 2. e' 4  |
		< e' gis' > 2. e' 4  |
		< e' a' > 2. e' 4  |
		< e' a' > 2. e' 4  |
%% 15
		< fis' a' > 2. fis' 4  |
		< fis' a' > 2. fis' 4  |
		< fis' b' > 1  |
		< fis' b' > 1  |
		R1*8  |
		a' 1  |
		b' 1  |
		cis'' 1  |
%% 30
		e'' 1  |
		a' 1  |
		b' 1  |
		cis'' 1  |
		gis' 1  |
%% 35
		a' 1  |
		e'' 4 dis'' cis'' dis''  |
		e'' 1  |
		R1*9  |
		a' 1  |
		b' 1  |
		cis'' 1  |
%% 50
		e'' 1  |
		a' 1  |
		b' 1  |
		cis'' 1  |
		gis' 1  |
%% 55
		a' 1  |
		e'' 4 dis'' cis'' dis''  |
		e'' 1  |
		r2 r8 a cis' e'  |
		a' 4. e' a' 4  |
%% 60
		fis' 4. e' dis' 4  |
		e' 4. fis' gis' 4  |
		e' 4. b gis 4  |
		a 4. e' a' 4  |
		fis' 4. e' dis' 4  |
%% 65
		e' 4. fis' gis' 4  |
		dis' 4. b gis 4  |
		a 2 r8 a' cis'' e''  |
		dis'' 4. e'' fis'' 4  |
		gis'' 4. fis'' e'' 4  |
%% 70
		e'' 4. dis'' b' 4  |
		a' 1  |
		b' 1  |
		cis'' 1  |
		gis' 1  |
%% 75
		a' 2 r8 a' cis'' e''  |
		e'' 4 dis'' cis'' dis''  |
		e'' 4. b' gis' 4  |
		e' 4. gis' b' 4  |
		a' 2 r8 a' cis'' e''  |
%% 80
		e'' 4 dis'' cis'' dis''  |
		e'' 4. b' gis' 4  |
		e' 4. gis' b' 4  |
		a' 2 r8 a' cis'' e''  |
		e'' 4 dis'' cis'' dis''  |
%% 85
		e'' 1  |
		R1  |
		\bar "|."
	}
>>
